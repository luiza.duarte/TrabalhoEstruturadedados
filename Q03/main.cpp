#include <iostream>

using namespace std;

const int tamanho = 3;

int matriz1[tamanho][tamanho];
int matriz2[tamanho][tamanho];
int somar[tamanho][tamanho];
int multi[tamanho][tamanho];
int i;
int j;

void inicializarMatriz(int mat1[tamanho][tamanho], int mat2[tamanho][tamanho], int tam){
        for(i = 0;i < tam;i++){
        cout << "Matriz 1" << endl;
        cout << "Linha: " << (i+1) << endl;
        for(j = 0;j < tam;j++){
           cin >> mat1[i][j];
        }
    }

    for(i = 0;i < tam;i++){
        cout << "Matriz 2" << endl;
        cout << "Linha: " << (i+1) << endl;
        for(j = 0;j < tam;j++){
             cin >> mat2[i][j];
        }
    }
}

void calcularMatriz(int mat1[tamanho][tamanho], int mat2[tamanho][tamanho], int tam){
      cout << "Matriz Um:" << endl;
      for(i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
             cout << mat1[i][j] << " ";
        }
        cout << endl;
      }

      cout << "\nMatriz Dois:" << endl;
      for(i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
             cout << mat2[i][j] << " ";
         }
        cout << endl;
      }

      cout << "Somando matrizes:" << endl;
      for(i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
             somar[i][j] = mat1[i][j] + mat2[i][j];
             cout << somar[i][j] << " ";
        }
        cout << endl;
      }

      cout << "Multiplicando matrizes:" << endl;
      for(i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
             multi[i][j] = mat1[i][j] * mat2[i][j];
             cout << multi[i][j] << " ";
        }
        cout << endl;
      }
}

int main()
{
    inicializarMatriz(matriz1, matriz2, tamanho);
    calcularMatriz(matriz1, matriz2, tamanho);

    system("pause");
}
