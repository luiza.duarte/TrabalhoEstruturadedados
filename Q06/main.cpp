#include <iostream>

using namespace std;

const int tamanho = 15;

int* vetor[tamanho];
int i;
int j;

void inicializarVetor(int* vet[], int tam){
    for(i=0; i < tam; i++){
        cout << "Digite um valor para a posicao " << (i+1) << ": " << endl;
        vet[i] = new int[i];
        cin >> *(vet[i]);
    }
}

void imprimirVetor(int* vet[], int tam){
    for ( i = tam - 1; i >= 0; i--){
        cout << "Valores Invertidos : " << *(vet[i]) << endl;
    }
}

int main()
{
    inicializarVetor(vetor, tamanho);
    imprimirVetor(vetor, tamanho);
    system("pause");
}
