#include <iostream>

using namespace std;

const int tamanho = 10;

int vetor[tamanho];
int maior;
int menor;
float media;
int i;

void iniciaVetor(int vet1[tamanho], int tam){
    cout << "Digite os "<< tam << " valores: " << endl;
    for(i = 0; i < tam; i++){
        cout << "Valor " << (i + 1) << ": ";
        cin >> vet1[i];
   }
   cout << endl;
}

void maiorValor(int vet1[tamanho], int tam){
    for(i = 0; i < tam; i++){
        if (vet1[i] > maior){
            maior = vet1[i];
        }
    }
   cout << "O maior valor e: " << maior << endl;
}

void menorValor(int vet1[tamanho], int tam){
    menor = vet1[i];
    for(i = 0; i < tam; i++){
        if (vet1[i] < menor){
            menor = vet1[i];
        }
    }
   cout << "O menor valor e: " << menor << endl;
}

void mediaVetor(int vet1[tamanho], int tam){
    for(i = 0; i < tam; i++){
        media = (vet1[i] + media);
    }
    media = media/tam;

   cout << "A media dos valores e: " << media << endl;
}
int main()
{
    iniciaVetor(vetor, tamanho);
    maiorValor(vetor, tamanho);
    menorValor(vetor, tamanho);
    mediaVetor(vetor, tamanho);

    system("pause");
}
