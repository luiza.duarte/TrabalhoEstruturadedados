#include <iostream>
#include <stdlib.h>

using namespace std;

const int tamanho = 5;

int i;

struct registro {
        int codigo;
        char nome[60];
        char endereco[60];
};

struct registro reg[tamanho];

void inicializarRegistro (int tam){
    for(i = 0; i < tam; i++){
    cout << "Digite o codigo: " << endl;
    cin >> reg[i].codigo;
    cout << "Digite o nome: " << endl;
    cin >> reg[i].nome;
    cout << "Digite o endereco: " << endl;
    cin >> reg[i].endereco;
    cout << " " << endl;
    }
}

void pesquisarStruct(int cod, int tam){
    for(i = 0; i < tam; i++){
        if(cod == reg[i].codigo){
            cout << "O nome : " << reg[i].nome << "." << endl;
            cout << "O endereco : " << reg[i].endereco << "." << endl;
            exit(0);
        }
    }
    cout << "Codigo n�o encontrado." << endl;
}

int main()
{
    int cod;

    inicializarRegistro(tamanho);
    cout << "Digite o codigo a ser pesquisar: " << endl;
    cin >> cod;
    pesquisarStruct(cod, tamanho);

    system("pause");
}
