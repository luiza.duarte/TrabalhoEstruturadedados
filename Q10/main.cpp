#include <iostream>

using namespace std;

const int tamanho = 10;

int i;
int maior;
int menor;
int vetor;
int valor;

struct pontos {
    int x;
    int y;
};

struct pontos p[tamanho];

void incrementarPontos (int tam){
    for(i=0; i < tam; i++){
    cout << "Insira o valor de X na posicao " << (i+1) << ": " << endl;
    cin >> p[i].x;
    }


    for(i=0; i < tam; i++){
    cout << "Insira o valor de Y na posicao " << (i+1) << ": " << endl;
    cin >> p[i].y;
    }
}

void maiorValordeX(pontos p[], int tam){
    for(i=0; i<tam; i++){
        if (p[i].x > maior){
            maior = p[i].x;
        }
    }
   cout << "O maior valor de X e: " << maior << endl;
}

void menorValorY(pontos p[], int tam){
    for(i=0; i<tam; i++){
        if (p[i].y < maior){
            menor = p[i].y;
        }
    }
   cout << "O menor valor de Y e: " << menor << endl;
}

void procurarValoremXeY(pontos p[], int tam){
    cout << "Digite o valor que devera ser pesquisado nos vetores X e Y:" << endl;
    cin >> valor;
    cout << endl;
    for(i=0; i<tam; i++){
        if(valor == p[i].x){
           cout << "As posicoes que contem o valor no vetor X:" << i << endl;
        }
    }
    for(i=0; i<tam; i++){
        if(valor == p[i].y){
            cout << "As posicoes que contem o valor no vetor Y:" << i << endl;
        }
    }
}

int main()
{
    incrementarPontos(tamanho);
    procurarValoremXeY(p, tamanho);
    maiorValordeX(p, tamanho);
    menorValorY(p, tamanho);

    system("pause");
}
